import datetime as dt

import numpy as np
import pandas as pd
import yfinance as yf
from matplotlib import pyplot as plt
from prophet import Prophet

# 'AAPL', 'TSLA', 'AMD', 'NVDA', 'BTC-USD', 'LINK-USD', 'ADA-USD'
ticker = 'BTC-USD'

# Predictions length
prediction_len = 5

# Log prices
log = False

dt_start = dt.datetime(1990, 1, 1)
dt_end = dt.datetime.now()

df = yf.download(ticker, start=dt_start, end=dt_end, group_by="ticker")
df.columns = ['open', 'high', 'low', 'close', 'adj_close', 'volume']

prophet_df = pd.DataFrame()
prophet_df[['ds', 'y']] = df.reset_index()[['Date', 'close']]

if log:
    prophet_df['y'] = np.log2(prophet_df['y'])

for index in range(-100, -30):
    data = prophet_df[index:index + 30]

    model = Prophet().fit(data)
    future = model.make_future_dataframe(periods=prediction_len)
    forecast = model.predict(future)
    forecast.set_index('ds', inplace=True)
    fig, ax = plt.subplots()
    forecast[['trend', 'yhat_lower', 'yhat_upper', 'trend_lower', 'trend_upper']].join(df['close']).plot(
        figsize=(12, 6), ax=ax)
    ax.axvline(prophet_df.iloc[index + 30].ds)
    plt.savefig(
        f'Predictions/{prophet_df.iloc[index].ds.strftime("%Y-%m-%d")}-{prophet_df.iloc[index - 30].ds.strftime("%Y-%m-%d")}')
