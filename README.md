# master-thesis

#### Repository containing source code for my master's thesis

## Info:

- all relevant code is located in src directory
    - main.py is a runnable script for fitting and evaluating models
- prophet directory is not relevant to this thesis (it contains code for Facebook prophet model)
- jupyter directory contains older code
- data directory contains clean filtered data

## Todo/Random ideas:

- TimeSeriesSplit, LSTM, CNN, XGBoost, Random Forest
- training model with multiple cryptocurrencies/stocks at once
- autoencoder for determining best indicators/parameters (principal component analysis <=)
- gridsearch for best parameters (scaling, indicators, prediction length and threshold, window size)
- parameter study to remove unnecessary features
- sanction model (bigger loss) when wrongfully predicting 1/-1 classes than class 0 (add custom weights to classes for
  learning penalty) <=
- give earnings data to model (3 features; 1-earnings difference, 2-days elapsed from last earnings, 3-expected
  earnings), marketcap,
- results and parameters of all models should be stored in a csv format, analyzed in dataframe
- google trends data
- make targets: <=
    - -1 if first low in next data is lower than threshold and there were no high prices that would trigger stoploss (
      all candles before had high value lower than stoploss)
    - 0 if first candle that had low or high breach threshold breach it with low and high price at the same time
    - 1 exactly opposite of -1
- Matrix of weights by return to penalise model (this way model wouldn't miss most important swings in the market) <=
    - weight should be calculated something like this:
    - ![img.png](img.png)
- remove end day data => solved with adding column time delta
- make threshold level equal to the volatility that was calculated in the window (i.e. in the last 15 days)

- weighted samples for learning,

https://www.cs.princeton.edu/sites/default/files/uploads/saahil_madge.pdf