import os

import pandas as pd

base_dir = './'
for file in os.listdir(base_dir):
    if ".csv" in file and "_1m" not in file:
        df = pd.read_csv(base_dir + file)
        df['date'] = pd.to_datetime(df['timestamp'], unit='s')
        df.drop('timestamp', axis=1, inplace=True)
        df.set_index('date', inplace=True)
        df.sort_index(inplace=True)
        df = df[['open', 'high', 'low', 'close', 'volume']]

        # Reindex DataFrame to add missing minutes
        df = df.reindex(pd.date_range(start=df.index.min(), end=df.index.max(), freq='min'))

        # Take only values from 9:30 to 16:00
        df = df.between_time('09:30', '16:00')

        # Remove all weekend data and holiday data (whole day is only nan values)
        df = df.groupby(df.index.date).filter(lambda x: not x.isnull().values.all())

        # Fill missing close values with previous existing value
        df['close'] = df['close'].fillna(method='ffill')

        # Fill missing OHL values with close
        df = df.transpose().fillna(method='bfill').transpose()

        # Fill missing volume data with 0
        df['volume'] = df['volume'].fillna(0)

        # Get exactly the same data (some rows have first few rows nan)
        df = df[(df.index > '2019-04-10') & (df.index <= '2021-04-5')]

        df.index.name = "date"

        df.to_csv("../" + file.split(".")[0] + "_1m.csv")
