import holidays as hd
import numpy as np
import pandas as pd
from talib import abstract


class IndicatorCreator:
    # If timeperiod is None, indicators will use default settings
    def __init__(self, timeperiod=None, open_diff=True):
        self.timeperiod = timeperiod
        self.open_diff = open_diff

    def create(self, data_df):
        if not isinstance(data_df, pd.DataFrame):
            raise Exception("Data must be given in a dataframe")

        data = {
            'open': data_df['open'].values.astype('float64'),
            'high': data_df['high'].values.astype('float64'),
            'low': data_df['low'].values.astype('float64'),
            'close': data_df['close'].values.astype('float64'),
            'volume': data_df['volume'].values.astype('float64')
        }
        data_df = data_df.copy()

        # Adding a column for percentage difference in open price
        if self.open_diff:
            data_df['open_diff'] = data_df['open'].pct_change()

        # Overlap studies
        data_df["SMA"] = abstract.SMA(data, timeperiod=21 if not self.timeperiod else self.timeperiod)
        data_df["EMA"] = abstract.EMA(data, timeperiod=21 if not self.timeperiod else self.timeperiod)
        data_df["WMA"] = abstract.WMA(data, timeperiod=21 if not self.timeperiod else self.timeperiod)
        data_df['BB_up'], data_df['BB_mid'], data_df['BB_low'] = \
            abstract.BBANDS(data, timeperiod=20 if not self.timeperiod else self.timeperiod, nbdevup=2., nbdevdn=2.)
        data_df["KAMA"] = abstract.KAMA(data)

        # Momentum Indicators
        data_df["CCI"] = abstract.CCI(data, timeperiod=20 if not self.timeperiod else self.timeperiod)
        data_df["MFI"] = abstract.MFI(data, timeperiod=14 if not self.timeperiod else self.timeperiod)
        data_df["ROC"] = abstract.ROC(data, timeperiod=9 if not self.timeperiod else self.timeperiod)
        data_df["RSI"] = abstract.RSI(data, timeperiod=14 if not self.timeperiod else self.timeperiod)
        data_df["WILLR"] = abstract.WILLR(data, timeperiod=14 if not self.timeperiod else self.timeperiod)

        # Volume Indicator Functions
        data_df["ADOSC"] = abstract.ADOSC(data, fastperiod=3, slowperiod=10)

        # Volatility Indicators
        data_df["ATR"] = abstract.ATR(data, timeperiod=14 if not self.timeperiod else self.timeperiod)

        # Statistic functions
        data_df["STDDEV"] = abstract.STDDEV(data, nbdev=1., timeperiod=20 if not self.timeperiod else self.timeperiod)

        return data_df.dropna()

    def create_multiple(self, data_dfs):
        return [self.create(data_df) for data_df in data_dfs]


class TimeFeatureCreator:
    def __init__(self, day_of_week=True, day_of_year=True, holidays=True, time_delta=True):
        self.day_of_week = day_of_week
        self.day_of_year = day_of_year
        self.holidays = holidays
        self.time_delta = time_delta

    def create(self, data_df):
        if not isinstance(data_df, pd.DataFrame):
            raise Exception("Data must be a pandas dataframe")

        if self.day_of_week:
            data_df['dow'] = data_df.index.dayofweek

        if self.day_of_year:
            data_df['doy'] = data_df.index.dayofyear

        if self.time_delta:
            data_df['time_delta'] = data_df.index.to_series().diff().astype("timedelta64[m]")

        if self.holidays:
            data_df['holidays'] = 0
            for country in hd.utils.list_supported_countries():
                country_holidays = hd.country_holidays(country)
                data_df['holidays'] += np.array([day in country_holidays for day in data_df.index], dtype=int)

        return data_df.dropna()

    def create_multiple(self, data_dfs):
        return [self.create(data_df) for data_df in data_dfs]
