import pandas as pd

from backtest import calculate_profit
from configs import run_config_triple_barrier, run_config_simple
from data import DataFrameDivider, SimpleWindowsTargetsCreator, TripleBarrierWindowsTargetsCreator
from feature_generators import IndicatorCreator, TimeFeatureCreator
from models import rf, svm, dnn, conv_nn, lstm_nn
from src.util import display_class_balance, get_dataframes, redirect_print_output, print_run, print_accuracy, \
    print_confusion_matrix, close_print_output

pd.set_option("display.max_rows", None, "display.max_columns", None)
pd.set_option('expand_frame_repr', False)


def prepare_data(run_config):
    real_dfs = get_dataframes(source=run_config['source'], tickers=run_config['tickers'],
                              aggregate=run_config['aggregate'])

    # Creating indicators
    indicator_creator = IndicatorCreator(timeperiod=run_config['indicator_timeperiod'],
                                         open_diff=run_config['open_diff'])
    real_dfs = indicator_creator.create_multiple(real_dfs)

    # Adding date/holiday info
    time_creator = TimeFeatureCreator(day_of_week=run_config['dow'], day_of_year=run_config['doy'],
                                      holidays=run_config['holidays'], time_delta=run_config['time_delta'])
    real_dfs = time_creator.create_multiple(real_dfs)

    # Dividing features with open price and adding a feature representing percentage move from last open price
    divider = DataFrameDivider(divider_col=run_config['divider_col'], cols=run_config['divided_cols'])
    divided_dfs = divider.divide_multiple(real_dfs)

    return real_dfs, divided_dfs


if __name__ == "__main__":
    for run_config in [run_config_simple, run_config_triple_barrier]:
        print(f"Current config: {run_config['strategy']}")

        real_dfs, data_dfs = prepare_data(run_config)

        creator = None
        if run_config['strategy'] == "simple":
            creator = SimpleWindowsTargetsCreator(window_length=run_config['window_length'],
                                                  target_offset=run_config['target_offset'],
                                                  threshold=run_config['threshold'],
                                                  target_column=run_config['target_column'])
        elif run_config['strategy'] == "triple_barrier":
            creator = TripleBarrierWindowsTargetsCreator(window_length=run_config['window_length'],
                                                         target_offset=run_config['target_offset'],
                                                         threshold=run_config['threshold'],
                                                         target_column=run_config['target_column'])
        x_train, y_train, x_test, y_test = \
            creator.create_from_multiple(real_dfs, data_dfs, run_config['tickers'], test_start=run_config['test_start'])
        display_class_balance(y_train, "y_train")
        display_class_balance(y_test, "y_test")
        for model, model_name in [(conv_nn, "CONV_NN")]:
            print(f"Current model: {model_name}")

            model = model(creator.cv_indices)
            model.fit(x_train, y_train)

            print_output = redirect_print_output(file='../results/results.txt')
            print_run(model, run_config, data_dfs)
            print("Cross validation results:")
            print(pd.DataFrame(model.cv_results_))
            print()
            display_class_balance(y_train, "y_train")
            display_class_balance(y_test, "y_test")
            y_test_prediction = model.predict(x_test)
            display_class_balance(y_test_prediction, "y_test_prediction")
            print_accuracy(y_test, y_test_prediction)
            print_confusion_matrix(y_test, y_test_prediction)

            combined_profit = 0
            for i, ticker in enumerate(run_config['tickers']):
                profit, trades_count = calculate_profit(model.predict(creator.test_windows[ticker]), real_dfs[i],
                                                        creator.test_target_dates[ticker], run_config)
                print(f'Trading strategy made {profit * 100:6.3f}% using predictions on {ticker} from '
                      f"{run_config['test_start'].strftime('%Y/%m/%d')} in {trades_count} trades.")
                combined_profit += profit
            print(f"Trading strategy made {combined_profit / len(run_config['tickers']) * 100:6.3f}% overall from "
                  f"{run_config['test_start'].strftime('%Y/%m/%d')}")

            close_print_output(*print_output)
