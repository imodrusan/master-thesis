from scikeras.wrappers import KerasClassifier
from sklearn import pipeline
from sklearn import preprocessing
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import StandardScaler, StandardScaler
from sklearn.svm import SVC
from tensorflow import keras

from pipeline_steps import WindowFlattener, WindowScaler


# from sklearn.utils import estimator_html_repr
# with open('my_estimator.html', 'w') as f:
#     f.write(estimator_html_repr(model))


def create_dnn(meta, hidden_layer_dim=50):
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]

    model = keras.models.Sequential()
    model.add(keras.layers.Dense(n_features_in_, input_shape=X_shape_[1:]))
    model.add(keras.layers.Dense(hidden_layer_dim))
    model.add(keras.layers.Activation("relu"))
    model.add(keras.layers.Dense(hidden_layer_dim))
    model.add(keras.layers.Activation("relu"))
    model.add(keras.layers.Dense(n_classes_))
    model.add(keras.layers.Activation("softmax"))
    return model


dnn = lambda cv: GridSearchCV(
    estimator=pipeline.Pipeline([
        ('window_flattener', WindowFlattener()),
        ('rescale', StandardScaler()),
        ('nn', KerasClassifier(create_dnn,
                               loss="sparse_categorical_crossentropy",
                               class_weight="balanced",
                               verbose=0,
                               epochs=10,
                               hidden_layer_dim=100))
    ]),
    param_grid={'nn__hidden_layer_dim': [10, 50, 100],
                'nn__epochs': [5, 20, 50]},
    cv=cv)

rf = lambda cv: GridSearchCV(
    estimator=pipeline.Pipeline([
        ('window_flattener', WindowFlattener()),
        ('rf', RandomForestClassifier(class_weight='balanced', random_state=7))
    ]),
    param_grid={'rf__max_depth': [5, 10, 20, 1000]},
    cv=cv)

svm = lambda cv: GridSearchCV(
    estimator=pipeline.Pipeline([
        ('window_flattener', WindowFlattener()),
        ('rescale', StandardScaler()),
        ('svc', SVC(class_weight='balanced', random_state=7))
    ]),
    param_grid={'svc__kernel': ["rbf"],
                'svc__C': [0.5, 1, 5]},
    cv=cv)


def create_conv_nn(meta, filters=[8, 4], hidden_layer_dim=100):
    # note that meta is a special argument that will be
    # handed a dict containing input metadata
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]

    model = keras.models.Sequential()
    model.add(keras.layers.Reshape((*X_shape_[1:], 1), input_shape=X_shape_[1:]))
    model.add(keras.layers.Conv2D(filters=filters[0], kernel_size=(4, 4), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Conv2D(filters=filters[1], kernel_size=(3, 3), activation='relu'))
    model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))
    model.add(keras.layers.Flatten())
    model.add(keras.layers.Dense(32))
    model.add(keras.layers.Activation("relu"))
    model.add(keras.layers.Dense(n_classes_))
    model.add(keras.layers.Activation("softmax"))
    return model


conv_nn = lambda cv: GridSearchCV(
    estimator=pipeline.Pipeline([
        ('scaler', WindowScaler(StandardScaler())),
        ('conv_nn', KerasClassifier(create_conv_nn,
                                    loss="sparse_categorical_crossentropy",
                                    class_weight="balanced",
                                    filters=[8, 4],
                                    verbose=0,
                                    epochs=10,
                                    hidden_layer_dim=100))
    ]),
    param_grid={'conv_nn__filters': [[8, 8], [4, 4]],
                'conv_nn__epochs': [5, 20, 50]},
    cv=cv)


def create_lstm(meta, units=32):
    # note that meta is a special argument that will be
    # handed a dict containing input metadata
    n_features_in_ = meta["n_features_in_"]
    X_shape_ = meta["X_shape_"]
    n_classes_ = meta["n_classes_"]

    model = keras.models.Sequential()
    model.add(keras.layers.LSTM(units, input_shape=X_shape_[1:]))
    model.add(keras.layers.Dense(n_classes_))
    model.add(keras.layers.Activation("softmax"))
    return model


lstm_nn = lambda cv: GridSearchCV(
    estimator=pipeline.Pipeline([
        ('scaler', WindowScaler(StandardScaler())),
        ('lstm_nn', KerasClassifier(create_lstm,
                                    loss="sparse_categorical_crossentropy",
                                    class_weight="balanced",
                                    units=16,
                                    verbose=0,
                                    epochs=10))
    ]),
    param_grid={'lstm_nn__units': [64, 16, 8],
                'lstm_nn__epochs': [5, 20, 50]},
    cv=cv)
