import numpy as np
from sklearn.base import BaseEstimator, TransformerMixin


class WindowScaler(BaseEstimator, TransformerMixin):
    def __init__(self, scaler):
        self.scaler = scaler

    def fit(self, X, y=None):
        for x in X:
            self.scaler = self.scaler.partial_fit(x)
        return self

    def transform(self, X, y=None):
        return np.array([self.scaler.transform(x) for x in X])


class WindowFlattener(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        if isinstance(X, np.ndarray):
            if all(isinstance(x, np.ndarray) for x in X):
                return self

        raise Exception("Data must be given in a list of numpy arrays")

    def transform(self, X, y=None):
        return np.array([x.flatten() for x in X])


# Used to stop and check values in pipe in debugger
class PipeLogger(BaseEstimator, TransformerMixin):
    def __init__(self):
        pass

    def fit(self, X, y=None):
        return self

    def transform(self, X, y=None):
        return X
