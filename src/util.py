import os
import sys

import numpy as np
import pandas as pd
import yfinance as yf
from matplotlib import pyplot as plt


def display_class_balance(targets, variable_name):
    classifications, occurrences = np.unique(targets, return_counts=True)

    print(f"{variable_name} class balance:")
    for i, classification in enumerate(classifications):
        print(f"\t- class {classification:2d} occurred {occurrences[i]:7d} times"
              f" ({occurrences[i] / len(targets) * 100:.2f}%)")

    print()


def score_performance(y, prediction, classes=[-1, 1]):
    correct = 0
    all_predictions = 0
    for i in range(len(y)):
        if y[i] in classes:
            all_predictions += 1
            if y[i] == prediction[i]:
                correct += 1

    return correct / all_predictions if all_predictions > 0 else 0


def plot_predictions(real_dfs, data_dfs, tickers, period, creator, model):
    for i, data_df in enumerate(data_dfs):
        data_to_predict = creator.create_windows(data_df.iloc[-period - creator.window_length:])
        prediction = model.predict(data_to_predict)

        test_df = real_dfs[i][['close']].iloc[-period - 1:]
        test_df['prediction'] = prediction

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        test_df[['close']].plot(figsize=(17, 6), ax=ax1, color='k', ylabel="Price")
        test_df[['prediction']].plot(drawstyle="steps-post", figsize=(17, 6), ax=ax2, ylabel="Buy/Sell signals")
        ax1.set_title(tickers[i])
        plt.show()


def redirect_print_output(file='./results.txt', append=True):
    orig_stdout = sys.stdout
    f = open(file, 'a' if append else 'w')
    sys.stdout = f
    return f, orig_stdout


def close_print_output(f, orig_stdout):
    sys.stdout = orig_stdout
    f.close()


def print_run(model, run_config, dfs):
    print(f"\n====================================================================")
    print('Config:')
    print(f"\t- Target labeling strategy: {run_config['strategy']}")
    print(f"\t- Tickers: {run_config['tickers']}")
    print(f"\t- Data source: {run_config['source']}")
    print(f"\t- Data period: {run_config['aggregate']}")
    print(f"\t- Indicator timestamp: {run_config['indicator_timeperiod']}")
    print(f"\t- Window length: {run_config['window_length']}")
    print(f"\t- Target offset: {run_config['target_offset']}")
    print(f"\t- Threshold: {run_config['threshold']}")
    print(f"\t- Test data start: {run_config['test_start'].strftime('%Y/%m/%d')}")
    print(f"\t- Pipeline: {model}")
    print(f"\t- Features: {[feature for feature in dfs[0].columns]}")
    print(f"\t- Trading fee: {run_config['trading_fee']}")
    print('\nData:')
    for i, df in enumerate(dfs):
        print(f"\t- {run_config['tickers'][i]} available data: "
              f"{df.index.min().strftime('%Y/%m/%d')} - {df.index.max().strftime('%Y/%m/%d')}")
    print()


def print_accuracy(real_targets, predicted_targets):
    print("Model achieved accuracy of:")
    print(f"\t- {score_performance(real_targets, predicted_targets, [0, 1, -1]) * 100:3.2f}% overall")
    print(f"\t- {score_performance(real_targets, predicted_targets, [1]) * 100:3.2f}% on class  1")
    print(f"\t- {score_performance(real_targets, predicted_targets, [0]) * 100:3.2f}% on class  0")
    print(f"\t- {score_performance(real_targets, predicted_targets, [-1]) * 100:3.2f}% on class -1\n")


def print_confusion_matrix(y_test, y_test_prediction):
    df = pd.DataFrame({'y_test': y_test, 'y_test_prediction': y_test_prediction}, columns=['y_test', 'y_test_prediction'])

    confusion_matrix = pd.crosstab(df['y_test'], df['y_test_prediction'], rownames=['y_test'], colnames=['y_test_prediction'])
    print(confusion_matrix)
    print()


def get_dataframes(source="yahoo", tickers=['F'], aggregate=None):
    dataframes = []
    # Download daily data from yahoo
    if source == "yahoo":
        for ticker in tickers:
            df = yf.download(ticker, period="max", group_by="ticker", progress=False)
            df.columns = [col.lower() for col in df.columns]

            # removing first 5 trading days because they are an anomaly (high volume and fluctuations)
            df = df.iloc[5:]

            if aggregate:
                df = aggregate_df(df, aggregate)

            dataframes.append(df)

    # Read data from local directory
    else:
        for file_name in os.listdir(source):
            if any(ticker + "_" in file_name for ticker in tickers):
                df = pd.read_csv(os.path.join(source, file_name), index_col='date')
                df.index = pd.to_datetime(df.index)
                df.sort_index()

                if aggregate:
                    df = aggregate_df(df, aggregate)

                dataframes.append(df)

    return dataframes


# Aggregate dataframe data: '30T', '1h', '1d', '1w'
def aggregate_df(df, aggregate="30T"):
    ohlc_dict = {
        'open': 'first',
        'close': 'last',
        'high': 'max',
        'low': 'min',
        'volume': 'sum',
    }

    return df.resample(aggregate, closed='left', label='left').apply(ohlc_dict).dropna()
