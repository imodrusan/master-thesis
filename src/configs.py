import datetime as dt

import pandas as pd

run_config_simple = {
    'strategy': "simple",
    'tickers': ['AAPL', 'MSFT', 'AMZN', 'TSLA', 'GOOGL'],
    'source': '../data',
    'aggregate': None,
    'time_diff': pd.Timedelta('00:01:01'),
    'window_length': 15,
    'target_offset': 5,
    'threshold': 0.00016,
    'indicator_timeperiod': None,
    'dow': True,
    'doy': True,
    'holidays': False,
    'time_delta': False,
    'open_diff': True,
    'divider_col': 'open',
    'divided_cols': ['open', 'close', 'high', 'low', 'SMA', 'EMA', 'WMA', 'BB_up', 'BB_low', 'BB_mid', 'KAMA',
                     'STDDEV'],
    'target_column': 'close',
    'test_start': dt.datetime(2021, 1, 1),
    'trading_fee': 0.00,
}

run_config_triple_barrier = {
    'strategy': "triple_barrier",
    'tickers': ['AAPL', 'MSFT', 'AMZN', 'TSLA', 'GOOGL'],
    'source': '../data',
    'aggregate': None,
    'time_diff': pd.Timedelta('00:01:01'),
    'window_length': 15,
    'target_offset': 5,
    'threshold': 0.00035,
    'indicator_timeperiod': None,
    'dow': True,
    'doy': True,
    'holidays': False,
    'time_delta': False,
    'open_diff': True,
    'divider_col': 'open',
    'divided_cols': ['open', 'close', 'high', 'low', 'SMA', 'EMA', 'WMA', 'BB_up', 'BB_low', 'BB_mid', 'KAMA',
                     'STDDEV'],
    'target_column': 'close',
    'test_start': dt.datetime(2021, 1, 1),
    'trading_fee': 0.00,
}
