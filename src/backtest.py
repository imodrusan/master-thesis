import numpy as np
import pandas as pd
from matplotlib import pyplot as plt


def calculate_display_trades_profitability(real_dfs, data_dfs, run_config, creator, model):
    # Calculating profitability over test data and displaying trades
    for real_df, data_df, ticker in zip(real_dfs, data_dfs, run_config['tickers']):
        data_df = data_df[run_config['test_start']:]
        real_df = real_df[run_config['test_start']:]

        trading_df = pd.DataFrame()
        trading_df['price'] = real_df.iloc[run_config['window_length'] - 1:][run_config['target_column']]
        trading_df['prediction'] = model.predict(creator.create_windows(data_df))
        trading_df['returns'] = creator.get_returns(real_df)
        trading_df['target'] = np.append(creator.create_targets(real_df), [None] * run_config['target_offset'])

        profit = calculate_profit(trading_df, run_config)
        print(f'Trading strategy made {profit * 100:6.3f}% using prediction on {ticker} from '
              f"{trading_df.index.min().strftime('%Y/%m/%d')} to {trading_df.dropna().index.max().strftime('%Y/%m/%d')}")

        draw_trading(trading_df, ticker)


def calculate_profit(predictions, real_df, entry_dates, run_config):
    profit = 1
    trades_count = 0
    current_position = predictions[0]
    entry_price = real_df.loc[entry_dates[0]]['close']
    for prediction, prediction_date in zip(predictions, entry_dates):
        if prediction != current_position:
            exit_price = real_df.loc[prediction_date]['close']
            if current_position != 0:
                pct_diff = (exit_price - entry_price) / entry_price
                profit *= (current_position * pct_diff + 1 - run_config['trading_fee'])
                trades_count += 1
            entry_price = exit_price
            current_position = prediction

    return profit - 1, trades_count

# def calculate_profit(predictions, returns, run_config):
#     profit = 1
#     for prediction, return_pct in zip(predictions, returns):
#         if prediction != 0:
#             profit *= (1 + prediction * return_pct - run_config['trading_fee'])
#
#     return profit - 1


def draw_trading(trading_df, ticker):
    fig, ax1 = plt.subplots()
    ax2 = ax1.twinx()
    trading_df[['target']].plot(drawstyle="steps-post", figsize=(17, 6), ax=ax2, color='g', linestyle='dotted',
                                ylabel="Real target")
    trading_df[['prediction']].plot(drawstyle="steps-post", figsize=(17, 6), ax=ax2, color='b', linestyle='dotted',
                                    ylabel="Buy/Sell signals")
    trading_df[['price']].plot(figsize=(17, 6), ax=ax1, color='k', ylabel="Price", logy=True)
    ax1.set_title(ticker)
    plt.show()
