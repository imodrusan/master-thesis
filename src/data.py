import datetime as dt

import numpy as np
import pandas as pd
from sklearn.model_selection import TimeSeriesSplit


def compare(x1, x2, rtol):
    if np.isclose(x1, x2, rtol=rtol):
        return 0
    if x1 > x2:
        return -1
    return 1


class BaseWindowsTargetsCreator:
    def __init__(self, window_length=15, target_offset=5, threshold=0.01, target_column='close'):
        self.window_length = window_length
        self.target_column = target_column
        self.target_offset = target_offset
        self.threshold = threshold
        self.test_returns = dict()
        self.test_windows = dict()
        self.cv_splitter = TimeSeriesSplit(gap=self.window_length, n_splits=4)
        self.cv_indices = []
        self.test_target_dates = {}

    def create_targets_windows(self, real_df, data_df, ticker, save_target_dates):
        raise Exception("Please override")

    def create_from_multiple(self, real_dfs, data_dfs, tickers, test_start=dt.datetime(2021, 1, 1)):
        x_train = []
        y_train = []
        x_test = []
        y_test = []
        indices_index = 0

        for real_df, data_df, ticker in zip(real_dfs, data_dfs, tickers):
            x_train_temp, y_train_temp, train_returns_temp = self.create_targets_windows(real_df[:test_start],
                                                                                         data_df[:test_start],
                                                                                         ticker, save_target_dates=False)
            x_test_temp, y_test_temp, test_returns_temp = self.create_targets_windows(real_df[test_start:],
                                                                                      data_df[test_start:],
                                                                                      ticker, save_target_dates=True)

            if not self.cv_indices:
                self.cv_indices = list(self.cv_splitter.split(x_train_temp))
            else:
                for i, (train_indices, validation_indices) in enumerate(self.cv_splitter.split(x_train_temp)):
                    self.cv_indices[i] = (
                        np.concatenate((self.cv_indices[i][0], train_indices + indices_index), axis=None),
                        np.concatenate((self.cv_indices[i][1], validation_indices + indices_index), axis=None))
            indices_index += len(x_train_temp)

            self.test_returns[ticker] = test_returns_temp
            self.test_windows[ticker] = x_test_temp

            x_train.append(x_train_temp)
            y_train.append(y_train_temp)
            x_test.append(x_test_temp)
            y_test.append(y_test_temp)

        return np.concatenate(x_train, axis=0), np.concatenate(y_train, axis=0), \
               np.concatenate(x_test, axis=0), np.concatenate(y_test, axis=0),


# For creating windows, targets, returns and sample weights for learning for simple strategy.
# Strategy has three classes, -1, 0 or 1 depending on price movement (return) and threshold provided
class SimpleWindowsTargetsCreator(BaseWindowsTargetsCreator):
    def create_targets_windows(self, real_df, data_df, ticker, save_target_dates=False):
        if not isinstance(data_df, pd.DataFrame):
            raise Exception("Data must be given in a dataframe")

        if self.window_length + self.target_offset > len(data_df):
            raise Exception("Data is too short to create windows and targets with wanted arguments")

        windows = []
        targets = []
        returns = []

        for date, day_df in data_df.groupby(data_df.index.date):
            for i in range(0, (len(day_df) + 1) - self.window_length - self.target_offset, self.target_offset):
                window = day_df[i: i + self.window_length + self.target_offset]
                window_real = real_df.loc[window.index, :]

                # if (window['volume'] > 0).sum() > self.window_length / 2:
                windows.append(window.iloc[:-self.target_offset].values)

                entry_price = window_real.iloc[self.window_length - 1][self.target_column]
                exit_price = window_real.iloc[-1][self.target_column]
                pct_diff = (exit_price - entry_price) / entry_price
                returns.append(pct_diff)

                if save_target_dates:
                    if ticker not in self.test_target_dates:
                        self.test_target_dates[ticker] = []
                    self.test_target_dates[ticker].append(window_real.index[self.window_length - 1])

                if pct_diff > self.threshold:
                    targets.append(1)
                elif pct_diff < -self.threshold:
                    targets.append(-1)
                else:
                    targets.append(0)

        return windows, targets, returns


# For creating windows, targets, returns and sample weights for learning for triple barrier.
# Strategy has three classes, -1, 0 or 1 depending on price movement (return) and threshold provided
class TripleBarrierWindowsTargetsCreator(BaseWindowsTargetsCreator):
    def create_targets_windows(self, real_df, data_df, ticker, save_target_dates=False):
        if not isinstance(data_df, pd.DataFrame):
            raise Exception("Data must be given in a dataframe")

        if self.window_length + self.target_offset > len(data_df):
            raise Exception("Data is too short to create windows and targets with wanted arguments")

        windows = []
        targets = []
        returns = []
        for date, day_df in data_df.groupby(data_df.index.date):
            for i in range(0, (len(day_df) + 1) - self.window_length - self.target_offset, self.target_offset):
                window = day_df[i: i + self.window_length + self.target_offset]
                window_real = real_df.loc[window.index, :]

                # if (window['volume'] > 0).sum() > self.window_length / 2:
                windows.append(window.iloc[:-self.target_offset].values)

                if save_target_dates:
                    if ticker not in self.test_target_dates:
                        self.test_target_dates[ticker] = []
                    self.test_target_dates[ticker].append(window_real.index[self.window_length - 1])

                target = 0
                entry_price = window_real.iloc[self.window_length - 1][self.target_column]
                top_barrier = entry_price * (1 + self.threshold)
                bottom_barrier = entry_price * (1 - self.threshold)
                exit_price = window_real.iloc[-1][self.target_column]
                for row in window_real[self.window_length:].itertuples():
                    if row.high > top_barrier and row.low < bottom_barrier:
                        target = 0
                        if row.close > top_barrier:
                            target = 1
                        elif row.close < bottom_barrier:
                            target = -1
                        exit_price = row.close
                        break
                    elif row.high > top_barrier:
                        target = 1
                        exit_price = top_barrier
                        break
                    elif row.low < bottom_barrier:
                        target = -1
                        exit_price = bottom_barrier
                        break

                pct_diff = (exit_price - entry_price) / entry_price
                returns.append(pct_diff)
                targets.append(target)

        return windows, targets, returns


class DataFrameDivider:
    def __init__(self, divider_col, cols):
        self.divider_col = divider_col
        self.cols = cols

    def divide(self, data_df):
        data_df = data_df.copy()

        data_df[self.cols] = data_df[self.cols].div(data_df[self.divider_col], axis=0)
        data_df.drop([self.divider_col], axis=1, inplace=True)
        return data_df

    def divide_multiple(self, data_dfs):
        return [self.divide(data_df) for data_df in data_dfs]
